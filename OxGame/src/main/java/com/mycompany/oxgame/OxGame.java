/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.oxgame;

/**
 *
 * @author Win10
 */
import java.util.Scanner;

public class OxGame {
    private char[][] board;
    private char currentPlayer;
    
    public void initializeBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }
    
    public void printBoard() {
        System.out.println("-------------");

        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            System.out.println("-------------");
        }
    }
    
    public boolean isBoardFull() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == '-') {
                    return false;
                }
            }
        }
        return true;
    }
    
    public OxGame() {
        board = new char[3][3];
        currentPlayer = 'X';
        initializeBoard();
    }
    
    public boolean isWinningMove(int row, int col) {
        // Check row
        if (board[row][0] == board[row][1] && board[row][1] == board[row][2] && board[row][0] != '-') {
            return true;
        }

        // Check column
        if (board[0][col] == board[1][col] && board[1][col] == board[2][col] && board[0][col] != '-') {
            return true;
        }

        // Check diagonals
        if (row == col) {
            if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != '-') {
                return true;
            }
        }

        if (row + col == 2) {
            if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != '-') {
                return true;
            }
        }

        return false;
    }
    
    public void changePlayer() {
        if (currentPlayer == 'X') {
            currentPlayer = 'O';
        } else {
            currentPlayer = 'X';
        }
    }
    
    public boolean makeMove(int row, int col) {
        if (row >= 0 && row < 3 && col >= 0 && col < 3 && board[row][col] == '-') {
            board[row][col] = currentPlayer;
            return true;
        }
        return false;
    }
    
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.println("Welcome to Ox game");
        
        while (true) {
            OxGame game = new OxGame();
            int row, col;
            boolean gameEnded = false;

            while (!gameEnded) {
                game.printBoard();

                System.out.print("Player " + game.currentPlayer + ", enter your move (row [0-2] and column [0-2]): ");
                row = scanner.nextInt();
                col = scanner.nextInt();

                if (game.makeMove(row, col)) {
                    if (game.isWinningMove(row, col)) {
                        game.printBoard();
                        System.out.println("Player " + game.currentPlayer + " wins!");
                        gameEnded = true;
                    } else if (game.isBoardFull()) {
                        game.printBoard();
                        System.out.println("It's a draw!");
                        gameEnded = true;
                    } else {
                        game.changePlayer();
                    }
                } else {
                    System.out.println("Invalid move. Try again.");
                }
                
                System.out.print("Do you want to play again? (yes/no): ");
                String playAgain = scanner.next();

                if (!playAgain.equalsIgnoreCase("yes")) {
                break;
                }
                scanner.close();
            }
        }
    }
}
